# encoding: utf-8

import os

HOST = "0.0.0.0"
PORT = 5001
SECRET_KEY = "No secret key"

JINJA_ENV = {
    "TRIM_BLOCKS": True,
    "LSTRIP_BLOCKS": True,
}

# defining base directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# defining database URI
SQLALCHEMY_DATABASE_URI = "mysql://secours:secours@localhost/secours"
SQLALCHEMY_TRACK_MODIFICATIONS = False

SEND_FILE_MAX_AGE_DEFAULT = 0
