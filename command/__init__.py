from command.createsuperuser import createsuperuser
from command.migration import migrate


commands = (createsuperuser, migrate)
