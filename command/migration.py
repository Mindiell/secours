# encoding: utf-8

import base64
import bcrypt
import click
from flask.cli import with_appcontext
import hashlib

from app.model.user import User
from app.model.poste import Poste


@click.group()
@click.pass_context
def migrate(context):
    "Update databse if necessary"
    context.ensure_object(dict)


@migrate.command(help="Update database")
@click.pass_context
@with_appcontext
def migration(context):
    click.echo("Starting database update")
    for user in User.query.all():
        user.password = bcrypt.hashpw(
            base64.b64encode(hashlib.sha256(user.password.encode("utf-8")).digest()),
            bcrypt.gensalt(),
        ).decode("utf-8")
        user.save()
    for poste in Poste.query.all():
        if poste.statut == "opened":
            poste.statut = "ouvert"
        elif poste.statut == "closed":
            poste.statut = "fermé"
        elif poste.statut == "canceled":
            poste.statut = "annulé"
        poste.save()
    click.echo("Database updated!")
