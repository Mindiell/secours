"""
Server module.

Initializes Flask application and extensions and runs it.
"""

import flask
from flask import session

from app import admin, db, login_manager, migrate
from app.model import *
from app.routes import routes
from command import commands


app = flask.Flask(__name__, template_folder="app/view")
app.config.from_object("settings")
app.jinja_env.trim_blocks = app.config["JINJA_ENV"]["TRIM_BLOCKS"]
app.jinja_env.lstrip_blocks = app.config["JINJA_ENV"]["LSTRIP_BLOCKS"]

# Loading routes
for route in routes:
    if len(route) < 3:
        app.add_url_rule(route[0], route[1].__name__, route[1], methods=["GET"])
    else:
        app.add_url_rule(
            route[0],
            route[1].__name__,
            route[1],
            methods=route[2],
        )

# Initialisation of extensions
admin.init_app(app)
db.init_app(app)
login_manager.init_app(app)
migrate.init_app(app, db)

# Manage commands
for command in commands:
    app.cli.add_command(command)

# Manage user
login_manager.login_view = "user.login"


# Loading user
@login_manager.user_loader
def load_user(user_id):
    """
    Load user from its id.
    """
    return user.get_user(user_id)
