# encoding: utf-8

from app.controller.admin import Admin
from app.controller.core import Core
from app.controller.user import User
from app.controller.poste import Poste
from app.controller.file import File

routes = [
    # Page d'accueil
    ("/", Core.as_view("home")),
    # Aide
    ("/help", Core.as_view("help")),
    # Connexion au compte
    ("/login", User.as_view("login"), ["POST"]),
    # Déconnexion
    ("/logout", User.as_view("logout")),
    # Profil
    ("/profile", User.as_view("profile"), ["GET", "POST"]),
    # Parametres
    ("/parameters", User.as_view("parameters"), ["GET", "POST"]),
    # Gestion des postes
    ("/post/list", Poste.as_view("list"), ["GET", "POST"]),
    # Ajout d'un poste de secours
    ("/post/add", Poste.as_view("add")),
    # Edition d'un poste de secours
    ("/post/<int:poste_id>/edit", Poste.as_view("edit"), ["GET", "POST"]),
    # Duplication d'un poste de secours
    ("/post/<int:poste_id>/duplicate", Poste.as_view("duplicate")),
    # Annulation d'un poste de secours
    ("/post/<int:poste_id>/cancel", Poste.as_view("cancel")),
    # Clôture d'un poste de secours
    ("/post/<int:poste_id>/close", Poste.as_view("close")),
    # Ouverture d'un poste de secours
    ("/post/<int:poste_id>/open", Poste.as_view("open")),
    # Suppression d'un poste de secours
    ("/post/<int:poste_id>/delete", Poste.as_view("delete")),
    # Téléchargement d'un fichier pour un poste de secours
    ("/file/<string:model>/post/<int:poste_id>", File.as_view("download")),
    # Peut-être remplacer cet accès par l'accès "lambda" qui ajouterait un lien si je suis un admin ?
    ("/admin/login", Admin.as_view("login"), ["GET", "POST"]),
]

apis = []
