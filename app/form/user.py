# encoding: utf-8

from flask_wtf import FlaskForm
from wtforms import StringField, FileField, SelectField, HiddenField, PasswordField
from wtforms.validators import DataRequired


class UserForm(FlaskForm):
    user_id = HiddenField("Id")
    login = StringField("Identifiant", validators=[DataRequired()])
    password = PasswordField("Mot de passe", validators=[DataRequired()])
    password_confirm = PasswordField("Confirmation du mot de passe")
    email = StringField("Adresse e-mail")


class ParameterForm(FlaskForm):
    user_id = HiddenField("Id")
    password = PasswordField("Mot de passe")
    password_confirmation = PasswordField("Confirmation")
    lignes = StringField("Lignes affichées")
