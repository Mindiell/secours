# encoding: utf-8

from flask_wtf import FlaskForm
from wtforms import StringField, FileField, SelectField, HiddenField, PasswordField
from wtforms.validators import DataRequired

from app.form.poste import SEXES


class UlForm(FlaskForm):
    ul_id = HiddenField("Id")
    nom = StringField("Nom")
    code = StringField("Code")
    adresse = StringField("Adresse", render_kw={"size": "60"})
    code_postal = StringField("Code postal")
    ville = StringField("Ville")
    site_web = StringField("Site web", render_kw={"size": "60"})
    president_sexe = SelectField("", coerce=int, choices=SEXES, default=0)
    president_prenom = StringField("", render_kw={"placeholder": "Prénom"})
    president_nom = StringField("", render_kw={"placeholder": "Nom"})
    dlus_sexe = SelectField("", coerce=int, choices=SEXES, default=0)
    dlus_prenom = StringField("", render_kw={"placeholder": "Prénom"})
    dlus_nom = StringField("", render_kw={"placeholder": "Nom"})
    dlus_telephone = StringField("", render_kw={"placeholder": "Téléphone"})
    action_prenom = StringField("", render_kw={"placeholder": "Prénom"})
    action_nom = StringField("", render_kw={"placeholder": "Nom"})
    action_mail = StringField("", render_kw={"size": "30", "placeholder": "Mail"})
    action_telephone = StringField("", render_kw={"placeholder": "Téléphone"})
