# encoding: utf-8

from flask_wtf import FlaskForm
from wtforms import (
    DateField,
    FieldList,
    FloatField,
    FormField,
    HiddenField,
    IntegerField,
    SelectField,
    StringField,
    TextAreaField,
)
from wtforms.validators import DataRequired

SEXES = (
    (1, "M"),
    (2, "Mme"),
)

GENRES = (
    (1, "le"),
    (2, "la"),
    (3, "l'"),
    (4, "les"),
    (5, ""),
)

PUBLICS = (
    (
        (0, ""),
        (1, "Un Point d'Alerte et de Premiers Secours (PAPS)"),
        (2, "Une équipe de secours avec PSA"),
        (3, "Une équipe de secours avec VPSP"),
        (4, "Le RIS étant inférieur à 0,25"),
    ),
    (
        (0, ""),
        (1, "Une équipe d'intervention"),
        (2, "Une équipe d'intervention avec VPSP"),
        (3, "Un binôme de secouristes"),
        (4, "Une équipe VTT"),
        (5, "Une équipe d'évacuation avec VPSP"),
        (6, "Il n'est pas nécessaire de mettre en place un DPS"),
    ),
    (
        (0, ""),
        (1, "Une équipe d'évacuation avec VPSP"),
        (2, "Un binôme de secouristes"),
        (3, "Deux binômes de secouristes"),
        (4, "Une équipe VTT"),
        (5, "Deux équipes VTT"),
        (6, "Un binôme de secouristes et une équipe VTT"),
    ),
    (
        (0, ""),
        (1, "Une équipe d'évacuation avec VPSP"),
    ),
)

ACTEURS = (
    (
        (0, ""),
        (1, "Une équipe de secours avec PSA"),
        (2, "Une équipe de secours avec VPSP"),
        (3, "Il n'est pas nécessaire de mettre en place un DPS"),
    ),
    (
        (0, ""),
        (1, "Une équipe d'intervention"),
        (2, "Une équipe d'intervention avec VPSP"),
        (3, "Un binôme de secouristes"),
        (4, "Une équipe VTT"),
        (5, "Une équipe d'évacuation avec VPSP"),
    ),
    (
        (0, ""),
        (1, "Une équipe d'évacuation avec VPSP"),
        (2, "Un binôme de secouristes"),
        (3, "Deux binômes de secouristes"),
        (4, "Une équipe VTT"),
        (5, "Deux équipes VTT"),
        (6, "Un binôme de secouristes et une équipe VTT"),
    ),
    (
        (0, ""),
        (1, "Une équipe d'évacuation avec VPSP"),
    ),
)

EFFECTIFS = (
    (
        (0, ""),
        (1, "un chef d'intervention"),
        (2, "deux chefs d'intervention"),
        (3, "trois chefs d'intervention"),
        (4, "un équipier secouriste"),
    ),
    (
        (0, ""),
        (1, "un chauffeur VPSP"),
        (2, "deux chauffeurs VPSP"),
        (3, "trois chauffeurs VPSP"),
        (4, "un équipier secouriste"),
        (5, "deux équipiers secouristes"),
        (6, "trois équipiers secouristes"),
        (7, "quatre équipiers secouristes"),
        (8, "cinq équipiers secouristes"),
        (9, "six équipiers secouristes"),
    ),
    (
        (0, ""),
        (1, "un équipier secouriste"),
        (2, "deux équipiers secouristes"),
        (3, "trois équipiers secouristes"),
        (4, "quatre équipiers secouristes"),
        (5, "cinq équipiers secouristes"),
        (6, "six équipiers secouristes"),
        (7, "un secouriste"),
        (8, "deux secouristes"),
        (9, "trois secouristes"),
        (10, "quatre secouristes"),
    ),
    (
        (0, ""),
        (1, "un secouriste"),
        (2, "deux secouristes"),
        (3, "trois secouristes"),
        (4, "quatre secouristes"),
    ),
)

MATERIELS = (
    (
        (0, ""),
        (1, "Une VL"),
        (2, "Deux VL"),
        (3, "trois VL"),
        (4, "un VPSP"),
        (5, "un VPSP et une VL"),
        (6, "un VPSP et deux VL"),
        (7, "deux VPSP"),
        (8, "deux VPSP et une VL"),
        (9, "deux VPSP et deux VL"),
        (10, "trois VPSP"),
        (11, "trois VPSP et une VL"),
        (12, "trois VPSP et deux VL"),
    ),
    (
        (0, ""),
        (1, "deux VTT"),
        (2, "quatre VTT"),
        (3, "un lot A"),
        (4, "deux lots A"),
        (5, "trois lots A"),
        (6, "un lot C"),
    ),
    (
        (0, ""),
        (1, "Une VL"),
        (2, "Deux VL"),
        (3, "deux VTT"),
        (4, "quatre VTT"),
        (5, "un lot A"),
        (6, "deux lots A"),
        (7, "trois lots A"),
        (8, "un lot B"),
        (9, "deux lots B"),
        (10, "un lot C"),
    ),
    (
        (0, ""),
        (1, "un lot B"),
        (2, "deux lots B"),
        (3, "un lot C"),
    ),
    (
        (0, ""),
        (1, "un lot C"),
    ),
)


class AffectationEntryForm(FlaskForm):
    nom = StringField("", render_kw={"placeholder": "Nom"})
    nivol = StringField("", render_kw={"placeholder": "NIVOL"})
    qualification = StringField("", render_kw={"placeholder": "CDP/CI/CH/PSE2"})
    ul = StringField("", render_kw={"placeholder": "78XX"})


class PosteForm(FlaskForm):
    ul_id = HiddenField("Ul_id")
    tab = HiddenField("")
    nom = StringField(
        "", validators=[DataRequired()], render_kw={"placeholder": "Nom", "size": "61"}
    )
    ville = StringField("", render_kw={"placeholder": "Ville"})
    date = StringField(
        "", validators=[DataRequired()], render_kw={"placeholder": "Date"}
    )
    date_debut = StringField("", render_kw={"size": "20"})
    heure_debut = StringField("", render_kw={"size": "4"})
    date_fin = StringField("", render_kw={"size": "20"})
    heure_fin = StringField("", render_kw={"size": "4"})
    adresse_depart = StringField("Adresse de départ :", render_kw={"size": "61"})
    adresse_arrivee = StringField("Adresse d'arrivée :", render_kw={"size": "61"})
    activites = StringField("Activités :")
    organisateur_type = SelectField("", coerce=int, choices=GENRES, default=0)
    organisateur_nom = StringField(
        "", render_kw={"placeholder": "Nom organisateur", "size": "61"}
    )
    organisateur_adresse = StringField("Adresse :", render_kw={"size": "61"})
    organisateur_denomination = StringField("Dénomination :", render_kw={"size": "61"})
    representant_sexe = SelectField("", coerce=int, choices=SEXES, default=0)
    representant_prenom = StringField("", render_kw={"placeholder": "Prénom"})
    representant_nom = StringField("", render_kw={"placeholder": "Nom"})
    representant_fonction = StringField("", render_kw={"placeholder": "Fonction"})
    contact_sexe = SelectField("", coerce=int, choices=SEXES, default=0)
    contact_prenom = StringField("", render_kw={"placeholder": "Prénom"})
    contact_nom = StringField("", render_kw={"placeholder": "Nom"})
    contact_fonction = StringField("", render_kw={"placeholder": "Fonction"})
    contact_telephone = StringField("", render_kw={"placeholder": "Téléphone"})
    dimensionnement_indice = FloatField("", render_kw={"size": "4"}, default=0)
    dimensionnement_ris = FloatField("", render_kw={"size": "4"}, default=0)
    dimensionnement_public_1 = SelectField(
        "", coerce=int, choices=PUBLICS[0], default=0
    )
    dimensionnement_public_2 = SelectField(
        "", coerce=int, choices=PUBLICS[1], default=0
    )
    dimensionnement_public_3 = SelectField(
        "", coerce=int, choices=PUBLICS[2], default=0
    )
    dimensionnement_public_4 = SelectField(
        "", coerce=int, choices=PUBLICS[3], default=0
    )
    dimensionnement_acteur_1 = SelectField(
        "", coerce=int, choices=ACTEURS[0], default=0
    )
    dimensionnement_acteur_2 = SelectField(
        "", coerce=int, choices=ACTEURS[1], default=0
    )
    dimensionnement_acteur_3 = SelectField(
        "", coerce=int, choices=ACTEURS[2], default=0
    )
    dimensionnement_acteur_4 = SelectField(
        "", coerce=int, choices=ACTEURS[3], default=0
    )
    dimensionnement_effectif_1 = SelectField(
        "", coerce=int, choices=EFFECTIFS[0], default=0
    )
    dimensionnement_effectif_2 = SelectField(
        "", coerce=int, choices=EFFECTIFS[1], default=0
    )
    dimensionnement_effectif_3 = SelectField(
        "", coerce=int, choices=EFFECTIFS[2], default=0
    )
    dimensionnement_effectif_4 = SelectField(
        "", coerce=int, choices=EFFECTIFS[3], default=0
    )
    dimensionnement_materiel_1 = SelectField(
        "", coerce=int, choices=MATERIELS[0], default=0
    )
    dimensionnement_materiel_2 = SelectField(
        "", coerce=int, choices=MATERIELS[1], default=0
    )
    dimensionnement_materiel_3 = SelectField(
        "", coerce=int, choices=MATERIELS[2], default=0
    )
    dimensionnement_materiel_4 = SelectField(
        "", coerce=int, choices=MATERIELS[3], default=0
    )
    dimensionnement_materiel_5 = SelectField(
        "", coerce=int, choices=MATERIELS[4], default=0
    )
    paps_quantite = IntegerField("", render_kw={"size": "3"}, default=0)
    dps_quantite = IntegerField("", render_kw={"size": "3"}, default=0)
    binome_quantite = IntegerField("", render_kw={"size": "3"}, default=0)
    evac_quantite = IntegerField("", render_kw={"size": "3"}, default=0)
    facturation_type = SelectField("", coerce=int, choices=GENRES, default=0)
    facturation_nom = StringField("", render_kw={"size": "61"})
    facturation_adresse_1 = StringField("", render_kw={"size": "61"})
    facturation_adresse_2 = StringField("", render_kw={"size": "61"})
    facturation_code_postal = StringField("", render_kw={"size": "6"})
    facturation_ville = StringField("")
    facturation_reference = StringField("Référence")
    facturation_reference_crf = StringField("Référence CRf")
    facturation_agrement = StringField("Agrément")
    facturation_dossier = StringField("Dossier")
    facturation_montant = IntegerField("Montant", default=0)
    facturation_remise = IntegerField("Réduction", default=0)
    notes = TextAreaField("", render_kw={"rows": "6", "cols": "60"})
    statut = HiddenField("")
    affectations = FieldList(FormField(AffectationEntryForm), min_entries=1)
