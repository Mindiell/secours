# encoding: utf-8

import base64
import bcrypt
from flask import render_template, g, url_for, redirect, flash, session
from flask_login import login_user, logout_user, current_user, login_required
import hashlib

from app.controller.controller import Controller
from app.form.user import UserForm, ParameterForm
from app.form.ul import UlForm
from app.model.user import User as UserModel
from app.model.ul import Ul as UlModel


class User(Controller):
    def login(self):
        g.form = UserForm()
        if g.form.validate_on_submit():
            user = UserModel.query.filter_by(login=g.form.login.data).first()
            if user and user.check_password(g.form.password.data):
                if user.is_active:
                    login_user(user)
                    if user.admin:
                        return redirect(url_for("admin.index"))
                    return redirect(url_for("poste.list"))
                else:
                    flash("Compte inactif.")
            else:
                flash("Mauvais identifiant ou mot de passe.")
        for field, message in g.form.errors.items():
            flash("%s: %s" % (field, message[0]))
        return redirect(url_for("core.home"))

    def logout(self):
        logout_user()
        session.clear()
        return redirect(url_for("core.home"))

    @login_required
    def profile(self):
        g.ul = UlModel.query.get(current_user.ul_id)
        g.form = UlForm(obj=g.ul)
        if g.form.validate_on_submit():
            g.form.populate_obj(g.ul)
            g.ul.save()
            flash("Profil enregistré", "success")
        return render_template("user/profile.html")

    @login_required
    def parameters(self):
        g.user = UserModel.query.get(current_user.id)
        g.form = ParameterForm(obj=g.user)
        if g.form.validate_on_submit():
            g.user.lignes = g.form.lignes.data
            if g.form.password.data != "":
                if g.form.password.data != g.form.password_confirmation.data:
                    flash("Les mots de passe doivent être identiques")
                    return render_template("user/parameters.html")
                g.user.password = g.form.password.data
            g.user.save()
            flash("Nouveaux paramètres enregistrés", "success")
        return render_template("user/parameters.html")
