# encoding: utf-8

import locale
import os
import time

from flask import redirect, url_for, send_file, flash
from flask_login import login_required
from relatorio.templates.opendocument import Template

from app.controller.controller import Controller
from app.model.poste import Poste

from app.form.poste import PUBLICS, ACTEURS, EFFECTIFS, MATERIELS


TEMPORARY_PATH = "/tmp/"
MONTANT_PAPS = 265
MONTANT_DPS = 575
MONTANT_BINOME = 265
MONTANT_EVAC = 575
PRESIDENT_CRF = "son Président, Monsieur Philippe DACOSTA"
PRESIDENT_DT = "le Président Départemental des Yvelines, Monsieur Pascal CORMIER"


class File(Controller):
    @login_required
    def download(self, model, poste_id):
        poste = Poste.query.get(poste_id)
        if poste is None:
            return redirect(url_for("poste.list"))
        # Ajout de la date du jour
        locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")
        poste.today = time.strftime("%A %d %B %Y", time.gmtime())
        # Ajout des infos CRF
        poste.president_crf = PRESIDENT_CRF
        poste.president_dt = PRESIDENT_DT

        # Gestion du responsable de poste
        affectations = poste.affectations.all()
        if len(affectations) > 0:
            poste.responsable = {
                "nom": affectations[0].nom,
                "nivol": affectations[0].nivol,
                "qualification": affectations[0].qualification,
            }
        else:
            poste.responsable = {
                "nom": "",
                "nivol": "",
                "qualification": "",
            }

        # Création des dimensionnements
        poste.dimensionnement = {
            "public": [],
            "acteur": [],
            "effectif": [],
            "materiel": [],
        }
        if int(poste.dimensionnement_public_1) > 0:
            poste.dimensionnement["public"].append(
                {
                    "dimensionnement": PUBLICS[0][poste.dimensionnement_public_1][1]
                }
            )
        if int(poste.dimensionnement_public_2) > 0:
            poste.dimensionnement["public"].append(
                {
                    "dimensionnement": PUBLICS[1][poste.dimensionnement_public_2][1]
                }
            )
        if int(poste.dimensionnement_public_3) > 0:
            poste.dimensionnement["public"].append(
                {
                    "dimensionnement": PUBLICS[2][poste.dimensionnement_public_3][1]
                }
            )
        if int(poste.dimensionnement_public_4) > 0:
            poste.dimensionnement["public"].append(
                {
                    "dimensionnement": PUBLICS[3][poste.dimensionnement_public_4][1]
                }
            )

        if int(poste.dimensionnement_acteur_1) > 0:
            poste.dimensionnement["acteur"].append(
                {
                    "dimensionnement": ACTEURS[0][poste.dimensionnement_acteur_1][1]
                }
            )
        if int(poste.dimensionnement_acteur_2) > 0:
            poste.dimensionnement["acteur"].append(
                {
                    "dimensionnement": ACTEURS[1][poste.dimensionnement_acteur_2][1]
                }
            )
        if int(poste.dimensionnement_acteur_3) > 0:
            poste.dimensionnement["acteur"].append(
                {
                    "dimensionnement": ACTEURS[2][poste.dimensionnement_acteur_3][1]
                }
            )
        if int(poste.dimensionnement_acteur_4) > 0:
            poste.dimensionnement["acteur"].append(
                {
                    "dimensionnement": ACTEURS[3][poste.dimensionnement_acteur_4][1]
                }
            )
        if int(poste.dimensionnement_effectif_1) > 0:
            poste.dimensionnement["effectif"].append(
                {
                    "dimensionnement": EFFECTIFS[0][poste.dimensionnement_effectif_1][1]
                }
            )
        if int(poste.dimensionnement_effectif_2) > 0:
            poste.dimensionnement["effectif"].append(
                {
                    "dimensionnement": EFFECTIFS[1][poste.dimensionnement_effectif_2][1]
                }
            )
        if int(poste.dimensionnement_effectif_3) > 0:
            poste.dimensionnement["effectif"].append(
                {
                    "dimensionnement": EFFECTIFS[2][poste.dimensionnement_effectif_3][1]
                }
            )
        if int(poste.dimensionnement_effectif_4) > 0:
            poste.dimensionnement["effectif"].append(
                {
                    "dimensionnement": EFFECTIFS[3][poste.dimensionnement_effectif_4][1]
                }
            )

        if int(poste.dimensionnement_materiel_1) > 0:
            poste.dimensionnement["materiel"].append(
                {
                    "dimensionnement": MATERIELS[0][poste.dimensionnement_materiel_1][1]
                }
            )
        if int(poste.dimensionnement_materiel_2) > 0:
            poste.dimensionnement["materiel"].append(
                {
                    "dimensionnement": MATERIELS[1][poste.dimensionnement_materiel_2][1]
                }
            )
        if int(poste.dimensionnement_materiel_3) > 0:
            poste.dimensionnement["materiel"].append(
                {
                    "dimensionnement": MATERIELS[2][poste.dimensionnement_materiel_3][1]
                }
            )
        if int(poste.dimensionnement_materiel_4) > 0:
            poste.dimensionnement["materiel"].append(
                {
                    "dimensionnement": MATERIELS[3][poste.dimensionnement_materiel_4][1]
                }
            )
        if int(poste.dimensionnement_materiel_5) > 0:
            poste.dimensionnement["materiel"].append(
                {
                    "dimensionnement": MATERIELS[4][poste.dimensionnement_materiel_5][1]
                }
            )

        # Ajout des matériels et véhicules
        poste.materiels = ""
        if (
            poste.dimensionnement_materiel_2 is not None
            and int(poste.dimensionnement_materiel_2) > 0
        ):
            poste.materiels = MATERIELS[1][poste.dimensionnement_materiel_2][1]
        if (
            poste.dimensionnement_materiel_3 is not None
            and int(poste.dimensionnement_materiel_3) > 2
        ):
            poste.materiels = (
                f"{poste.materiels} + "
                f"{MATERIELS[2][poste.dimensionnement_materiel_3][1]}"
            )
        if (
            poste.dimensionnement_materiel_4 is not None
            and int(poste.dimensionnement_materiel_4) > 0
        ):
            poste.materiels = (
                f"{poste.materiels} + "
                f"{MATERIELS[3][poste.dimensionnement_materiel_4][1]}"
            )
        if (
            poste.dimensionnement_materiel_5 is not None
            and int(poste.dimensionnement_materiel_5) > 0
        ):
            poste.materiels = (
                f"{poste.materiels} + "
                f"{MATERIELS[4][poste.dimensionnement_materiel_5][1]}"
            )

        poste.vehicules = ""
        if (
            poste.dimensionnement_materiel_1 is not None
            and int(poste.dimensionnement_materiel_1) > 0
        ):
            poste.vehicules = MATERIELS[0][poste.dimensionnement_materiel_1][1]
        if (
            poste.dimensionnement_materiel_3 is not None
            and 0 < int(poste.dimensionnement_materiel_3) < 3
        ):
            poste.vehicules = (
                f"{poste.vehicules} + "
                f"{MATERIELS[2][poste.dimensionnement_materiel_3][1]}"
            )

        # Création des lignes de coût
        poste.devis = []
        if poste.paps_quantite > 0:
            poste.devis.append(
                {
                    "titre": "PAPS (Point d'Alerte et de Premiers Secours)",
                    "explication": "2 intervenants secouristes (dont 1 équipier "
                        "secouriste et 1 secouriste)\n1 VL + 1 lot C de secours",
                    "quantite": poste.paps_quantite,
                    "cout": MONTANT_PAPS,
                    "montant": MONTANT_PAPS * poste.paps_quantite,
                }
            )
        if poste.dps_quantite > 0:
            poste.devis.append(
                {
                    "titre": "DPS-PE (Dispositif Prévisionnel de Secours)",
                    "explication": "1 chef d'intervention et 3 intervenants "
                        "secouristes (dont 2 équipiers secouristes et 1 secouriste)"
                        " + 1 stagiaire\n1 VPSP + 1 lot A de secours",
                    "quantite": poste.dps_quantite,
                    "cout": MONTANT_DPS,
                    "montant": MONTANT_DPS * poste.dps_quantite,
                }
            )
        if poste.binome_quantite > 0:
            poste.devis.append(
                {
                    "titre": "Binôme(s) (rattaché au poste de secours)",
                    "explication": "2 intervenants secouristes (dont 1 équipier "
                        "secouriste et 1 secouriste)\n1 VL + 1 lot B de secours",
                    "quantite": poste.binome_quantite,
                    "cout": MONTANT_BINOME,
                    "montant": MONTANT_BINOME * poste.binome_quantite,
                }
            )
        if poste.evac_quantite > 0:
            poste.devis.append(
                {
                    "titre": "Équipe d'évacuation",
                    "explication": "1 chef d'intervention et 2 équipiers "
                        "secouristes\n1 VPSP + 1 lot A de secours",
                    "quantite": poste.evac_quantite,
                    "cout": MONTANT_EVAC,
                    "montant": MONTANT_EVAC * poste.evac_quantite,
                }
            )

        # Sélection du fichier
        filename = ""
        if model == "devis":
            document = Template(source="", filepath="models/devis.odt")
            filename = "2 - Devis - %s - %s.odt" % (poste.nom, poste.date)
        elif model == "convention":
            document = Template(source="", filepath="models/convention.odt")
            filename = "3 - Convention - %s - %s.odt" % (poste.nom, poste.date)
        elif model == "page de garde":
            document = Template(source="", filepath="models/page de garde.odt")
            filename = "4 - Page de garde - %s - %s.odt" % (poste.nom, poste.date)
        elif model == "ordre de mission":
            document = Template(source="", filepath="models/ordre de mission.odt")
            filename = "5 - Ordre de mission - %s - %s.odt" % (poste.nom, poste.date)
        elif model == "main courante":
            document = Template(source="", filepath="models/main courante.odt")
            filename = "7 - Main courante - %s - %s.odt" % (poste.nom, poste.date)
        elif model == "rapport de mission":
            document = Template(source="", filepath="models/rapport de mission.odt")
            filename = "11 - Rapport de mission - %s - %s.odt" % (poste.nom, poste.date)
        elif model == "facture":
            document = Template(source="", filepath="models/facture.odt")
            filename = "12 - Facture - %s - %s.odt" % (poste.nom, poste.date)

        # Génération et envoi du fichier
        generated = document.generate(p=poste).render()
        path = f"{TEMPORARY_PATH}{filename}"
        with open(path, "wb") as file_handler:
            file_handler.write(generated.getvalue())
        if os.path.isfile(path):
            return send_file(path, as_attachment=True)

        flash("Ce type de fichier est inconnu")

        return redirect(url_for("poste.edit", poste_id=poste.id))
