# encoding: utf-8

from flask import render_template, redirect, url_for, g, flash, request, session
from flask_login import current_user, login_required
import locale
import time

from app.controller.controller import Controller
from app.form.poste import PosteForm
from app.model.poste import Affectation as AffectationModel, Poste as PosteModel
from app.model.ul import Ul as UlModel


class Poste(Controller):
    @login_required
    def list(self):
        if request.method == "POST":
            session["order"] = request.form["order"]
            session["sorting"] = request.form["sorting"]
            session["filter"] = request.form["filter"]
            session["status"] = request.form["status"]
            if "ul_id" in request.form:
                session["ul_id"] = request.form["ul_id"]
            else:
                session["ul_id"] = ""
        else:
            session["order"] = "timestamp"
            session["sorting"] = "1"
            if "filter" not in session:
                session["filter"] = ""
            if "status" not in session:
                session["status"] = "ouvert"
            if "ul_id" not in session:
                session["ul_id"] = ""
        if current_user.admin:
            query = PosteModel.query
        else:
            query = PosteModel.query.filter_by(ul_id=current_user.ul_id)
        if session["status"] != "":
            query = query.filter_by(statut=session["status"])
        if session["filter"] != "":
            query = query.filter(
                PosteModel.nom.like("%" + session["filter"] + "%")
                | PosteModel.ville.like("%" + session["filter"] + "%")
                | PosteModel.organisateur_nom.like("%" + session["filter"] + "%")
            )
        if session["ul_id"] != "":
            query = query.filter_by(ul_id=session["ul_id"])
        if session["order"] != "":
            if session["sorting"] == "1":
                query = query.order_by(
                    getattr(PosteModel, session["order"]).desc(),
                )
            else:
                query = query.order_by(
                    getattr(PosteModel, session["order"]),
                )
        g.page = int(request.args.get("page", 1))
        g.postes = query.paginate(
            page=g.page, per_page=current_user.lignes, error_out=False
        )
        if g.postes.items == [] and g.page > 1:
            g.page = 1
            g.postes = query.paginate(
                page=g.page, per_page=current_user.lignes, error_out=False
            )
        if g.postes.total == current_user.lignes:
            g.max_pages = 1
        else:
            g.max_pages = int(g.postes.total / current_user.lignes) + 1
        g.uls = UlModel.query.all()

        return render_template("poste/list.html")

    @login_required
    def add(self):
        return redirect(url_for("poste.edit", poste_id=0))

    @login_required
    def edit(self, poste_id=0, tab="manifestation"):
        g.tab = request.args.get("tab", tab)
        g.poste_id = poste_id
        if poste_id > 0:
            g.poste = PosteModel.query.get(poste_id)
            if g.poste is None:
                return redirect(url_for("poste.list"))
            g.form = PosteForm(obj=g.poste)
        else:
            g.poste = PosteModel()
            g.poste.nom = ""
            g.poste.date = ""
            g.form = PosteForm()
        if g.form.validate_on_submit():
            if len(g.poste.affectations.all()) == 0:
                g.poste.affectations.append(AffectationModel())
            g.form.populate_obj(g.poste)
            if poste_id == 0:
                g.poste.ul_id = current_user.ul_id
                g.poste.statut = "ouvert"
            try:
                locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")
                g.poste.timestamp = time.mktime(time.strptime(g.poste.date, "%d %B %Y"))
            except:
                g.poste.timestamp = 0
            # Calcul automatique du montant
            g.poste.facturation_montant = g.poste.montant
            # Gestion des affectations vides
            for affectation in g.poste.affectations:
                if (
                    affectation.nom == "" and
                    affectation.nivol == "" and
                    affectation.qualification == "" and
                    affectation.ul == ""
                ):
                    affectation.delete()
            g.poste.save()
            if request.form.get("command", "") == "add_affectation":
                affectation = AffectationModel(poste=g.poste)
                affectation.save()
                return redirect(url_for("poste.edit", poste_id=g.poste.id, tab="affectations"))
            g.form.facturation_montant.data = g.poste.facturation_montant
            flash("Poste enregistré", "success")
            return redirect(
                url_for("poste.edit", poste_id=g.poste.id, tab=request.form["tab_id"])
            )
        else:
            for field, message in g.form.errors.items():
                flash("%s: %s" % (field, message[0]))

        return render_template("poste/edit.html")

    @login_required
    def duplicate(self, poste_id):
        poste = PosteModel.query.get(poste_id)
        new_poste = PosteModel()
        for key in poste.__dict__:
            if key != "id" and not key.startswith("_"):
                new_poste.__setattr__(key, poste.__getattribute__(key))
        new_poste.id = None
        new_poste.nom = f"{new_poste.nom} - dupliqué"
        try:
            locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")
            new_poste.timestamp = time.mktime(time.strptime(new_poste.date, "%d %B %Y"))
        except:
            new_poste.timestamp = 0
        new_poste.facturation_reference = ""
        new_poste.facturation_reference_crf = ""
        new_poste.facturation_agrement = ""
        new_poste.facturation_dossier = ""
        new_poste.facturation_montant = 0
        new_poste.facturation_remise = 0
        new_poste.statut = "ouvert"
        new_poste.save()

        return redirect(url_for("poste.edit", poste_id=new_poste.id))

    @login_required
    def open(self, poste_id):
        poste = PosteModel.query.get(poste_id)
        if poste is None:
            return redirect(url_for("poste.list"))
        poste.statut = "ouvert"
        poste.save()

        return redirect(url_for("poste.edit", poste_id=poste_id))

    @login_required
    def close(self, poste_id):
        poste = PosteModel.query.get(poste_id)
        if poste is None:
            return redirect(url_for("poste.list"))
        poste.statut = "fermé"
        poste.save()

        return redirect(url_for("poste.edit", poste_id=poste_id))

    @login_required
    def cancel(self, poste_id):
        poste = PosteModel.query.get(poste_id)
        if poste is None:
            return redirect(url_for("poste.list"))
        poste.statut = "annulé"
        poste.save()

        return redirect(url_for("poste.edit", poste_id=poste_id))

    @login_required
    def delete(self, poste_id):
        poste = PosteModel.query.get(poste_id)
        if poste is None:
            return redirect(url_for("poste.list"))
        poste.delete()

        return redirect(url_for("poste.list"))
