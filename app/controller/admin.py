# encoding: utf-8

import base64
import bcrypt
from flask import g, redirect, url_for, flash, session
from flask_login import login_user, logout_user
from flask_admin import BaseView, expose
import hashlib

from app import admin
from app.controller.controller import Controller
from app.form.user import UserForm
from app.model.user import User as UserModel


class Admin(Controller):
    def login(self):
        g.form = UserForm()
        if g.form.validate_on_submit():
            user = UserModel.query.filter_by(login=g.form.login.data).first()
            if user and user.check_password(g.form.password.data):
                if user.is_active:
                    login_user(user)
                    return redirect(url_for("admin.index"))
                else:
                    flash("Compte inactif.")
            else:
                flash("Mauvais identifiant ou mot de passe.")
        for field, message in g.form.errors.items():
            flash("%s: %s" % (field, message[0]))
        return redirect(url_for("admin.index"))


class LogoutView(BaseView):
    @expose("/")
    def index(self):
        logout_user()
        session.clear()
        return redirect(url_for("admin.index"))


# TODO: Display this link in Admin menu only if user is authenticated
admin.add_view(LogoutView(name="Logout", endpoint="logout"))
