# encoding: utf-8

from flask import render_template, g, redirect, url_for
from flask_login import current_user

from app.controller.controller import Controller
from app.form.user import UserForm


class Core(Controller):
    def home(self):
        if current_user.is_authenticated:
            return redirect(url_for("poste.list"))
        g.form = UserForm()
        return render_template("core/home.html")

    def help(self):
        return render_template("core/help.html")
