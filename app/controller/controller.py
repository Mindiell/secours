# encoding: utf-8

from flask import render_template


class Controller:
    @classmethod
    def as_view(cls, function_name, *class_args, **class_kwargs):
        # Create the view function to return
        def view(*args, **kwargs):
            self = view.view_class(*class_args, **class_kwargs)
            if hasattr(self, function_name):
                return getattr(self, function_name)(*args, **kwargs)
            else:
                return "Class %s has no method called %s" % (
                    cls.__name__,
                    function_name,
                )

        view.view_class = cls
        # name used for endpoint : Class name + function name
        view.__name__ = ".".join((cls.__name__.lower(), function_name))
        view.__doc__ = cls.__doc__
        view.__module__ = cls.__module__
        return view
