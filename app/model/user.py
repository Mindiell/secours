# encoding: utf-8

import bcrypt

from flask import current_app

from app import admin, db
from app.model.model import Model, View


def get_user(user_id):
    return User.query.get(user_id)


class User(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    # Unité locale liée
    ul_id = db.Column(db.Integer, db.ForeignKey("ul.id"))
    ul = db.relationship("Ul", backref=db.backref("comptes", lazy="dynamic"))
    # Données
    login = db.Column(db.String(200))
    password_hash = db.Column(db.String(128), default="")
    email = db.Column(db.String(200))
    active = db.Column(db.Boolean())
    admin = db.Column(db.Boolean())
    visitor = db.Column(db.Boolean())
    # Paramètres
    lignes = db.Column(db.Integer(), default=10)

    @property
    def password(self):
        return self.password_hash

    @password.setter
    def password(self, password):
        self.password_hash = bcrypt.hashpw(
            password.encode("utf-8"),
            bcrypt.gensalt(rounds=int(current_app.config["BCRYPT_ROUNDS"])),
        ).decode("UTF-8")

    def check_password(self, password):
        return bcrypt.checkpw(
            password.encode("utf-8"),
            self.password_hash.encode("utf-8"),
        )

    @property
    def is_active(self):
        return self.active or False

    @property
    def is_anonymous(self):
        return self.id is None

    @property
    def is_authenticated(self):
        return self.id is not None

    def get_id(self):
        return str(self.id)


class UserAdmin(View):
    column_exclude_list = [
        "password_hash",
    ]

    def on_model_change(self, form, model, is_created):
        if len(form.password_hash.data) < 128:
            model.password = form.password_hash.data


# Model will be automatically managed through flask-admin module
admin.add_view(UserAdmin(User, db.session))
