# encoding: utf-8

from app import admin, db
from app.model.model import Model, View


class Ul(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(200))
    code = db.Column(db.String(200))
    adresse = db.Column(db.String(200))
    code_postal = db.Column(db.String(200))
    ville = db.Column(db.String(200))
    site_web = db.Column(db.String(200))
    president_sexe = db.Column(db.Integer)
    president_prenom = db.Column(db.String(200))
    president_nom = db.Column(db.String(200))
    dlus_sexe = db.Column(db.Integer)
    dlus_prenom = db.Column(db.String(200))
    dlus_nom = db.Column(db.String(200))
    dlus_telephone = db.Column(db.String(200))
    action_prenom = db.Column(db.String(200))
    action_nom = db.Column(db.String(200))
    action_mail = db.Column(db.String(200))
    action_telephone = db.Column(db.String(200))

    def __repr__(self):
        return "%s - %s" % (self.nom, self.code)

    @property
    def president_mme(self):
        if self.president_sexe == 1:
            return "M."
        return "Mme"

    @property
    def president_fin(self):
        if self.president_sexe == 2:
            return "e"
        return ""

    @property
    def dlus_mme(self):
        if self.dlus_sexe == 1:
            return "M."
        return "Mme"

    @property
    def dlus_titre(self):
        if self.dlus_sexe == 1:
            return "M. le Directeur local"
        return "Mme la Directrice locale"


class UlAdmin(View):
    column_list = (
        "nom",
        "code",
    )


# Model will be automatically managed through flask-admin module
admin.add_view(UlAdmin(Ul, db.session))
