# encoding: utf-8

from app import admin, db
from app.model.model import Model, View

PAPS_MONTANT = 265
DPS_MONTANT = 575
BINOME_MONTANT = 265
EVAC_MONTANT = 575


class Poste(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    # Unité locale liée
    ul_id = db.Column(db.Integer, db.ForeignKey("ul.id"))
    ul = db.relationship("Ul", backref=db.backref("postes", lazy="dynamic"))
    # Données
    nom = db.Column(db.String(200))
    ville = db.Column(db.String(200))
    date = db.Column(db.String(200))
    timestamp = db.Column(db.Integer)
    date_debut = db.Column(db.String(200))
    heure_debut = db.Column(db.String(200))
    date_fin = db.Column(db.String(200))
    heure_fin = db.Column(db.String(200))
    adresse_depart = db.Column(db.String(200))
    adresse_arrivee = db.Column(db.String(200))
    activites = db.Column(db.String(200))
    organisateur_type = db.Column(db.Integer)
    organisateur_nom = db.Column(db.String(200))
    organisateur_adresse = db.Column(db.String(200))
    organisateur_denomination = db.Column(db.String(200))
    representant_sexe = db.Column(db.Integer)
    representant_nom = db.Column(db.String(200))
    representant_prenom = db.Column(db.String(200))
    representant_fonction = db.Column(db.String(200))
    contact_sexe = db.Column(db.Integer)
    contact_nom = db.Column(db.String(200))
    contact_prenom = db.Column(db.String(200))
    contact_fonction = db.Column(db.String(200))
    contact_telephone = db.Column(db.String(200))
    dimensionnement_indice = db.Column(db.Float)
    dimensionnement_ris = db.Column(db.Float)
    dimensionnement_public_1 = db.Column(db.Integer)
    dimensionnement_public_2 = db.Column(db.Integer)
    dimensionnement_public_3 = db.Column(db.Integer)
    dimensionnement_public_4 = db.Column(db.Integer)
    dimensionnement_acteur_1 = db.Column(db.Integer)
    dimensionnement_acteur_2 = db.Column(db.Integer)
    dimensionnement_acteur_3 = db.Column(db.Integer)
    dimensionnement_acteur_4 = db.Column(db.Integer)
    dimensionnement_effectif_1 = db.Column(db.Integer)
    dimensionnement_effectif_2 = db.Column(db.Integer)
    dimensionnement_effectif_3 = db.Column(db.Integer)
    dimensionnement_effectif_4 = db.Column(db.Integer)
    dimensionnement_materiel_1 = db.Column(db.Integer)
    dimensionnement_materiel_2 = db.Column(db.Integer)
    dimensionnement_materiel_3 = db.Column(db.Integer)
    dimensionnement_materiel_4 = db.Column(db.Integer)
    dimensionnement_materiel_5 = db.Column(db.Integer)
    paps_quantite = db.Column(db.Integer)
    dps_quantite = db.Column(db.Integer)
    binome_quantite = db.Column(db.Integer)
    evac_quantite = db.Column(db.Integer)
    facturation_type = db.Column(db.Integer)
    facturation_nom = db.Column(db.String(200))
    facturation_adresse_1 = db.Column(db.String(200))
    facturation_adresse_2 = db.Column(db.String(200))
    facturation_code_postal = db.Column(db.String(200))
    facturation_ville = db.Column(db.String(200))
    facturation_reference = db.Column(db.String(200))
    facturation_reference_crf = db.Column(db.String(200))
    facturation_agrement = db.Column(db.String(200))
    facturation_dossier = db.Column(db.String(200))
    facturation_montant = db.Column(db.Integer)
    facturation_remise = db.Column(db.Integer)
    notes = db.Column(db.Text())
    statut = db.Column(db.String(200))

    def __str__(self):
        return f"{self.nom} ({self.date})"

    @property
    def montant(self):
        montant_calcule = 0
        if self.paps_quantite is not None:
            montant_calcule += int("0%s" % self.paps_quantite) * PAPS_MONTANT
        if self.dps_quantite is not None:
            montant_calcule += int("0%s" % self.dps_quantite) * DPS_MONTANT
        if self.binome_quantite is not None:
            montant_calcule += int("0%s" % self.binome_quantite) * BINOME_MONTANT
        if self.evac_quantite is not None:
            montant_calcule += int("0%s" % self.evac_quantite) * EVAC_MONTANT
        if self.facturation_remise is not None:
            montant_calcule -= int("0%s" % self.facturation_remise)

        return montant_calcule

    @property
    def contact_mme(self):
        if self.contact_sexe == 1:
            return "M."
        return "Mme"

    @property
    def representant_mme(self):
        if self.representant_sexe == 1:
            return "M."
        return "Mme"

    @property
    def organisateur_determinant(self):
        if self.organisateur_type == 1:
            return "Le"
        elif self.organisateur_type == 2:
            return "La"
        elif self.organisateur_type == 3:
            return "L'"
        elif self.organisateur_type == 4:
            return "Les"
        return ""

    @property
    def organisateur_determinant2(self):
        if self.organisateur_type == 1:
            return "du"
        elif self.organisateur_type == 2:
            return "de la"
        elif self.organisateur_type == 3:
            return "de l'"
        elif self.organisateur_type == 4:
            return "des"
        return "du"

    @property
    def organisateur_fin(self):
        if self.organisateur_type == 2:
            return "e"
        elif self.organisateur_type == 4:
            return "s"
        return ""


class Affectation(db.Model, Model):
    id = db.Column(db.Integer, primary_key=True)
    # Poste lié
    poste_id = db.Column(db.Integer, db.ForeignKey("poste.id"))
    poste = db.relationship("Poste", backref=db.backref("affectations", lazy="dynamic"))
    nom = db.Column(db.String(200))
    nivol = db.Column(db.String(20))
    qualification = db.Column(db.String(20))
    ul = db.Column(db.String(20))


class PosteAdmin(View):
    column_list = (
        "ul",
        "nom",
        "ville",
        "date",
        "statut",
    )


class AffectationAdmin(View):
    column_list = (
        "poste",
        "nom",
        "nivol",
        "qualification",
        "ul",
    )


# Model will be automatically managed through flask-admin module
admin.add_view(PosteAdmin(Poste, db.session))
admin.add_view(AffectationAdmin(Affectation, db.session))
