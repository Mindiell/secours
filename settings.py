import os

from dotenv import load_dotenv


load_dotenv()


BASE_DIR = os.path.abspath(os.path.dirname(__file__))
DEBUG = os.environ.get("FLASK_DEBUG", False)
SECRET_KEY = os.environ.get("FLASK_SECRET_KEY", "Choose a secret key")
JINJA_ENV = {
    "TRIM_BLOCKS": True,
    "LSTRIP_BLOCKS": True,
}
SQLALCHEMY_DATABASE_URI = os.environ.get(
    "SQLALCHEMY_DATABASE_URI",
    "sqlite:///" + os.path.join(BASE_DIR, "db.sqlite3"),
)
SQLALCHEMY_TRACK_MODIFICATIONS = False
BCRYPT_ROUNDS = os.environ.get("BCRYPT_ROUNDS", 15)
